// Copyright 2021 Alberto Ruiz <aruiz@redhat.com>
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

#![no_std]
#![no_main]

extern crate alloc;
extern crate gpt_parser;

#[no_mangle]
extern "C" fn _Unwind_Resume() {}

use alloc::string::String;
use core::ffi::CStr;

use gpt_parser::{GPTHeader, GPTPartition, Uuid, LBA, LBA4K, LBA512};
use ieee1275::prom_init;
use ieee1275::services::Args;

use alloc::vec::Vec;

const BUFSIZE: usize = 10000;

fn ceiling(a: f64, b: f64) -> usize {
    assert!(a.is_normal());
    assert!(b.is_normal());
    assert!(a.is_sign_positive());
    assert!(b.is_sign_positive());
    assert_ne!(a, 0.0);
    assert_ne!(b, 0.0);

    let div = a / b;
    let div_int = div as usize;

    // We lack a % operator for floating points in no_std, we flip from u32 to check if it is an integer (x.0  number)
    // u32 conversion will always select the integer floor of the floating number
    if div == div_int as f64 {
        div_int
    } else {
        div_int + 1
    }
}

#[no_mangle]
#[link_section = ".text"]
extern "C" fn _start(_r3: u32, _r4: u32, entry: extern "C" fn(*mut Args) -> usize) -> isize {
    let prom = prom_init(entry);
    prom.write_line(String::from("Hello from Rust into Open Firmware\n\r").as_str());

    let mut buf: [u8; BUFSIZE] = [0; BUFSIZE];

    let _size = prom
        .get_property(prom.chosen, c"bootpath", &mut buf as *mut u8, buf.len())
        .unwrap();

    let dev_path_cstr = match CStr::from_bytes_until_nul(&buf) {
        Ok(dev_path) => dev_path,
        Err(_) => {
            prom.write_line("Could not get chosen bootpath property");
            prom.exit();
        }
    };
    let dev_path = dev_path_cstr.to_str().unwrap();

    let disk_handle = match prom.open(dev_path_cstr) {
        Err(msg) => {
            let _ = prom.write_stdout(dev_path);
            prom.write_line(msg);
            prom.exit();
        }
        Ok(file_handle) => {
            prom.write_line(dev_path);
            file_handle
        }
    };

    let block_size = match prom.get_block_size(disk_handle) {
        Ok(size) => {
            if size != 512 && size != 4096 {
                prom.write_line("Only block sizes of 512 or 4096 are supported");
                prom.exit()
            };
            size
        }
        Err(msg) => {
            prom.write_line(msg);
            prom.exit()
        }
    };

    let header = {
        let mut block_buffer_data: alloc::vec::Vec<u8> =
            alloc::vec::Vec::with_capacity(block_size as usize);

        /* We retreive the GPT LBA */
        if let Err(msg) = prom.seek(disk_handle, block_size as isize) {
            prom.write_line(msg);
            let _ = prom.close(disk_handle);
            prom.exit();
        }

        let _bytes_read = match prom.read(
            disk_handle,
            block_buffer_data.as_mut_ptr(),
            block_size as usize,
        ) {
            Err(msg) => {
                prom.write_line(msg);
                prom.exit();
            }
            Ok(size) => size,
        };

        let header = match block_size {
            512 => unsafe {
                GPTHeader::parse_gpt_header(
                    &mut *(block_buffer_data.as_mut_ptr() as *mut LBA<LBA512>),
                )
            },
            4096 => unsafe {
                GPTHeader::parse_gpt_header(
                    &mut *(block_buffer_data.as_mut_ptr() as *mut LBA<LBA4K>),
                )
            },
            _ => {
                prom.exit();
            }
        };

        match header {
            Ok(header) => header.clone(),
            Err(msg) => {
                prom.write_line(msg);
                prom.exit();
            }
        }
    };

    let part_size: u32 = header.part_size.into();
    let num_parts: u32 = header.num_parts.into();
    let parts_len = part_size * num_parts;
    let n_blocks = ceiling(parts_len as f64, block_size as f64) as usize;
    let part_data_len = block_size as usize * n_blocks;

    let partitions = {
        let mut block_buffer_data: alloc::vec::Vec<u8> =
            alloc::vec::Vec::with_capacity(part_data_len);

        let part_start_lba: u64 = header.part_start_lba.into();
        if let Err(msg) = prom.seek(disk_handle, block_size * part_start_lba as isize) {
            prom.write_line(msg);
            let _ = prom.close(disk_handle);
            prom.exit();
        }

        let _bytes_read =
            match prom.read(disk_handle, block_buffer_data.as_mut_ptr(), part_data_len) {
                Err(msg) => {
                    prom.write_line(msg);
                    prom.exit();
                }
                Ok(size) => size,
            };

        let part_array = unsafe {
            &*core::ptr::slice_from_raw_parts(
                block_buffer_data.as_mut_ptr() as *const GPTPartition,
                num_parts as usize,
            )
        };

        let part_size: u32 = header.part_size.into();
        let mut partitions: Vec<GPTPartition> = Vec::new();
        GPTHeader::parse_partitions(
            part_array,
            part_size as usize,
            |part, parts| {
                parts.push(part.clone());
            },
            &mut partitions,
        );

        partitions
    };

    let _ = partitions
        .iter()
        .filter(|p| p.part_type == Uuid::efi())
        .map(|p| {
            let first_lba: u64 = p.first_lba.into();
            if let Err(msg) = prom.seek(disk_handle, block_size * first_lba as isize) {
                prom.write_line(msg);
                let _ = prom.close(disk_handle);
                prom.exit();
            }

            buf = [0u8; 10000];

            let _bytes_read = match prom.read(disk_handle, buf.as_mut_ptr(), 1) {
                Err(msg) => {
                    prom.write_line(msg);
                    prom.exit();
                }
                Ok(size) => size,
            };

            prom.write_line(&String::from_utf8(buf[0..4].to_vec()).unwrap());

            return;
        });

    prom.write_line("Bye!");
    prom.exit();
}
