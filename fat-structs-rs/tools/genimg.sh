#!/bin/sh

# FAT32

function create_image () {
    local IMG=$1
    local IMGSIZE=$2
    local FATSIZE=$3
    local IMGID=$4

    rm -f $IMG
    fallocate -l $IMGSIZE $IMG
    mkfs.vfat -i $IMGID -F $FATSIZE $IMG > /dev/null 2>&1
    mmd -i $IMG EFI
    mmd -i $IMG EFI/FEDORA
    echo "content" | mcopy -i $IMG - ::EFI/FEDORA/somefilewithlargename.txt

    xz -c $IMG | base64 -w 0 - > base64.$IMG
}

create_image img32.img 100k 32 aaff55aa
create_image img16.img 9M   16 c125c3a6
create_image img12.img 100k 12 f08754a7