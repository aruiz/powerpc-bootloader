#![feature(exclusive_range_pattern)]
use explicit_endian::LittleEndian;

#[derive(Debug, PartialEq, Eq)]
pub enum FATType {
    FAT12,
    FAT16,
    FAT32,
}

#[derive(Debug, PartialEq, Clone, Copy)]
#[repr(packed, C)]
pub struct FSInfo {
    _lead_sig: [u8; 4],
    _reserved: [u8; 480],
    _struc_sig: [u8; 4],
    free_count: LittleEndian<u32>,
    nxt_free: LittleEndian<u32>,
    _reserved2: [u8; 12],
    _trail_sig: [u8; 4],
}

impl FSInfo {
    pub fn check_signatures(&self) -> bool {
        &self._lead_sig == b"RRaA"
            && &self._struc_sig == b"rrAa"
            && &self._trail_sig == &[0u8, 0u8, 0x55u8, 0xAAu8]
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
#[repr(packed, C)]
pub struct BaseVolumeBootRecord {
    _jmp_boot: [u8; 3],
    oem_name: [u8; 8],
    bytes_per_sector: LittleEndian<u16>,
    sectors_per_cluster: u8,
    reserved_sec_count: LittleEndian<u16>,
    num_fats: u8,
    root_entry_count: LittleEndian<u16>,
    total_sectors16: LittleEndian<u16>,
    media: u8,
    fat_size16: LittleEndian<u16>,
    sectors_per_track: LittleEndian<u16>,
    num_heads: LittleEndian<u16>,
    hidden_sectors: LittleEndian<u32>,
    total_sectors32: LittleEndian<u32>,
}

impl BaseVolumeBootRecord {
    fn get_fat_size(&self) -> u32 {
        let fatsz16: u16 = self.fat_size16.into();
        match fatsz16 == 0 {
            true => {
                let fat32vbr =
                    unsafe { &*{ self as *const BaseVolumeBootRecord as *const Fat32VBR } };
                fat32vbr.fat_size_32.into()
            }
            false => fatsz16 as u32,
        }
    }

    fn get_n_clusters(&self) -> usize {
        let sec_per_clust: u8 = self.sectors_per_cluster.into();
        let reserved_sectors: u16 = self.reserved_sec_count.into();
        let tot_sec16: u16 = self.total_sectors16.into();
        let tot_sec32: u32 = self.total_sectors32.into();
        let n_sectors: u32 = match tot_sec32 {
            0 => tot_sec16 as u32,
            _ => tot_sec32,
        };
        let fat_size = self.get_fat_size();
        let n_fats: u8 = self.num_fats.into();
        let data_sectors = n_sectors - reserved_sectors as u32 - (fat_size * n_fats as u32);

        (data_sectors / sec_per_clust as u32) as usize
    }

    fn is_fat_32(&self) -> bool {
        let fatsz16: u16 = self.fat_size16.into();

        // Regardless of the number of clusters, if FatSz16 is 0 then it's FAT32
        fatsz16 == 0
    }

    pub fn get_fat_type(&self) -> FATType {
        if self.is_fat_32() {
            return FATType::FAT32;
        }

        match self.get_n_clusters() {
            0..4084 => FATType::FAT12,
            4086..65524 => FATType::FAT16,
            _ => FATType::FAT32,
        }
    }

    pub fn get_cluster_size(&self) -> usize {
        let sec_per_clus = self.sectors_per_cluster;
        let bytes_per_sec: u16 = self.bytes_per_sector.into();
        bytes_per_sec as usize * sec_per_clus as usize
    }

    pub unsafe fn into<'a, T: VBR>(&'a self) -> Option<&'a T> {
        match self.get_fat_type() {
            FATType::FAT32 => match T::is_fat_32() {
                true => Some(&*(self as *const BaseVolumeBootRecord as *const T)),
                false => None,
            },
            _ => match T::is_fat_32() {
                true => None,
                false => Some(&*(self as *const BaseVolumeBootRecord as *const T)),
            },
        }
    }

    pub fn get_root_dir_offset(&self) -> isize {
        let res_sec_cnt: u16 = self.reserved_sec_count.into();
        let fat_size = self.get_fat_size();
        let bytes_per_sec: u16 = self.bytes_per_sector.into();

        let root_dir_sector: u32 = res_sec_cnt as u32 + (self.num_fats as u32 * fat_size as u32);

        let offset = root_dir_sector * bytes_per_sec as u32;
        offset as isize
    }

    pub fn get_first_data_cluster_offset(&self) -> isize {
        let rec: u16 = self.root_entry_count.into();
        let root_offset = self.get_root_dir_offset();

        root_offset + (rec as isize * 32)
    }
}

pub trait VBR {
    fn is_fat_32() -> bool;
}

pub trait FatArea {
    fn get_fat_area(&self) -> (isize, usize);
}

#[derive(Debug, PartialEq, Clone, Copy)]
#[repr(packed, C)]
pub struct VolData {
    drive_num: u8,
    _reserved: u8,
    ext_boot_sig: u8,
    vol_id: LittleEndian<u32>,
    vol_label: [u8; 11],
    file_system_type: [u8; 8],
}

#[derive(Debug, PartialEq)]
#[repr(packed, C)]
pub struct Fat1216VBR {
    base: BaseVolumeBootRecord,
    vol: VolData,
    boot_code: [u8; 448],
    boot_sig: [u8; 2],
}

#[derive(Debug, PartialEq, Clone, Copy)]
#[repr(packed, C)]
pub struct Fat32VBR {
    base: BaseVolumeBootRecord,
    fat_size_32: LittleEndian<u32>,
    extflags: LittleEndian<u16>,
    fs_ver: LittleEndian<u16>,
    root_cluster: LittleEndian<u32>,
    fs_info_sec: LittleEndian<u16>,
    bkp_boot_sec: LittleEndian<u16>,
    _reserved: [u8; 12],
    vol: VolData,
    boot_code: [u8; 420],
    boot_sig: [u8; 2],
}

impl Fat32VBR {
    pub fn check_signatures(&self) -> bool {
        self.boot_sig == [0x55, 0xaa] && self.vol.ext_boot_sig == 0x29
    }
}

impl VBR for Fat1216VBR {
    fn is_fat_32() -> bool {
        false
    }
}
impl VBR for Fat32VBR {
    fn is_fat_32() -> bool {
        true
    }
}

impl FatArea for Fat32VBR {
    fn get_fat_area(&self) -> (isize, usize) {
        let res_sec_cnt: u16 = self.base.reserved_sec_count.into();
        let bytes_per_sec: u16 = self.base.bytes_per_sector.into();
        let offset = res_sec_cnt * bytes_per_sec;
        let len: u32 = self.fat_size_32.into();
        (
            offset as isize,
            (len as u32 * bytes_per_sec as u32) as usize,
        )
    }
}

impl Fat1216VBR {
    pub fn check_signatures(&self) -> bool {
        self.boot_sig == [0x55, 0xaa] && self.vol.ext_boot_sig == 0x29
    }
}

impl FatArea for Fat1216VBR {
    fn get_fat_area(&self) -> (isize, usize) {
        let res_sec_cnt: u16 = self.base.reserved_sec_count.into();
        let bytes_per_sec: u16 = self.base.bytes_per_sector.into();
        let offset = res_sec_cnt * bytes_per_sec;
        let len: u16 = self.base.fat_size16.into();
        (offset as isize, (len * bytes_per_sec) as usize)
    }
}

#[repr(C, packed)]
#[derive(Debug, Clone, Copy)]
pub struct FAT32Entry(u32);

impl FAT32Entry {
    fn get_masked_value(&self) -> u32 {
        self.0 & 0x0FFFFFFF
    }
    pub fn is_free_cluster(&self) -> bool {
        self.get_masked_value() == 0
    }

    pub fn is_data_cluster(&self) -> bool {
        let masked_value = self.get_masked_value();
        masked_value >= 0x00000002 && masked_value <= 0x0ffffff5
    }

    pub fn is_end_of_chain(&self) -> bool {
        let masked_value = self.get_masked_value();
        masked_value >= 0x0ffffff8 || masked_value == 0x00000001
    }

    pub fn is_bad_cluster(&self) -> bool {
        self.get_masked_value() == 0x0ffffff7
    }
}

pub struct Fat32Area<'a>(&'a [u32]);

impl<'a> Fat32Area<'a> {}

#[derive(Debug, PartialEq, Eq)]
pub struct FAT16Entry(u16);

impl FAT16Entry {
    pub fn is_free_cluster(&self) -> bool {
        self.0 == 0x0000
    }

    pub fn is_data_cluster(&self) -> bool {
        self.0 >= 0x2 && self.0 <= 0xfff5
    }

    pub fn is_end_of_chain(&self) -> bool {
        self.0 == 0xffff
    }

    pub fn is_bad_cluster(&self) -> bool {
        self.0 == 0xfff7
    }
}

#[derive(Debug, PartialEq, Eq)]
#[repr(C, packed)]
pub struct DirEntry {
    pub name: [u8; 11],
    pub attr: u8,
    pub ntres: u8,
    pub crt_time_tenth: u8,
    pub crt_time: LittleEndian<u16>,
    pub crt_date: LittleEndian<u16>,
    pub lst_acc_date: LittleEndian<u16>,
    pub fst_clus_hi: LittleEndian<u16>,
    pub wrt_time: LittleEndian<u16>,
    pub wrt_date: LittleEndian<u16>,
    pub fst_clus_lo: LittleEndian<u16>,
    pub file_size: LittleEndian<u32>,
}

impl DirEntry {
    pub fn get_first_cluster(&self) -> u32 {
        let first_clus_hi: u16 = self.fst_clus_hi.into();
        let first_clus_lo: u16 = self.fst_clus_lo.into();
        let mut first_clus: u32 = first_clus_lo as u32;
        first_clus = first_clus | ((first_clus_hi as u32) << 16);
        first_clus
    }
}

pub struct DirEntries {
    pub clusters: Vec<u8>,
}

pub struct LFNDirEntry {
    pub ord: u8,
    pub name1: [u8; 10],
    pub attr: u8,
    pub entry_type: u8,
    pub chksum: u8,
    pub name2: [u8; 12],
    pub fstclusl0: LittleEndian<u16>,
    pub name3: [u8; 4],
}

pub struct FAT12Area<'a>(&'a [u8]);

#[derive(Debug, PartialEq, Eq)]
pub struct FAT12Entry(u16);

impl<'a> FAT12Area<'a> {
    pub fn len(&self) -> usize {
        self.0.len() * 2 / 3
    }

    pub fn get_entry(&self, i: usize) -> Option<FAT12Entry> {
        if i >= self.len() {
            return None;
        }

        let byte_index = i * 3 / 2;
        // If `i` it's even we use the first nibble in `self.0[byte_index]`
        let ret: u16 = match (i & 1) == 0 {
            // even
            true => {
                let mut ret = (self.0[byte_index] as u16) << 4;
                ret = ret | ((self.0[byte_index + 1] as u16) >> 4);
                ret
            }
            // odd
            _ => {
                let mut ret = ((self.0[byte_index] << 4) as u16) << 4;
                ret = ret | self.0[byte_index + 1] as u16;
                ret
            }
        };

        Some(FAT12Entry(ret))
    }
}

impl FAT12Entry {
    pub fn is_free_cluster(&self) -> bool {
        self.0 == 0x0000
    }

    pub fn is_data_cluster(&self) -> bool {
        self.0 >= 0x2 && self.0 <= 0xff5
    }

    pub fn is_end_of_chain(&self) -> bool {
        self.0 == 0xfff
    }

    pub fn is_bad_cluster(&self) -> bool {
        self.0 == 0x0ff7
    }
}

#[cfg(test)]
mod tests {
    use std::{mem::size_of, ptr::slice_from_raw_parts};

    use crate::{
        BaseVolumeBootRecord, DirEntry, FAT12Area, FAT12Entry, FAT16Entry, FAT32Entry, FATType,
        FSInfo, Fat1216VBR, Fat32VBR, FatArea,
    };

    fn decode_base64_xz_data(data: &str) -> Vec<u8> {
        use base64::{engine::general_purpose, Engine as _};
        use std::io::Read;

        let xz_fat_data = general_purpose::STANDARD.decode(data).unwrap();
        let mut decoder = xz::read::XzDecoder::new(xz_fat_data.as_slice());
        let mut fat_data: Vec<u8> = Vec::new();
        let _ = decoder.read_to_end(&mut fat_data);
        fat_data
    }

    const BOOT_CODE32: [u8; 29] = [
        0x0e, 0x1f, 0xbe, 0x77, 0x7c, 0xac, 0x22, 0xc0, 0x74, 0x0b, 0x56, 0xb4, 0x0e, 0xbb, 0x07,
        0x00, 0xcd, 0x10, 0x5e, 0xeb, 0xf0, 0x32, 0xe4, 0xcd, 0x16, 0xcd, 0x19, 0xeb, 0xfe,
    ];
    const BOOT_CODE16: [u8; 29] = [
        0x0e, 0x1f, 0xbe, 0x5b, 0x7c, 0xac, 0x22, 0xc0, 0x74, 0x0b, 0x56, 0xb4, 0x0e, 0xbb, 0x07,
        0x00, 0xcd, 0x10, 0x5e, 0xeb, 0xf0, 0x32, 0xe4, 0xcd, 0x16, 0xcd, 0x19, 0xeb, 0xfe,
    ];
    const BOOT_STR: [u8; 100] = *b"This is not a bootable disk.  Please insert a bootable floppy and\r\npress any key to try again ... \r\n";

    // This payload was generated using $ fallocate -l 100k tmp.img && mkfs.vfat -F 32 tmp.img && xz tmp.img && base64 -w 0 tmp.img.xz
    const FAT32_B64: &str = "/Td6WFoAAATm1rRGAgAhARYAAAB0L+Wj4Y//AXddAHWWDgbTdHlS2mDxS/egj1O/SkdEELYUGLAlOI5uagO6F58u+EgREHtlZzrYysbYbPpCroIthKYj2Q97Ie1btADh/tnTNisMqwTFiIaxInBEFiygfSncZFgpufHbOZcMB441MrcWXJ7iG41wZn4v1rwH2k0xj/utSC0pPHWQ3unPezYchxtbtp/SFYPxOY2QYiPnqE+RhJYrzzLk3l4Rc7gELyhbAVwDoGSu1ve2Cme80nadl9idscNw+GSC0oHTq+gEK5VnJON6W50CvUmfkuXt08E0ubuErN9LOx1zGTwAZz/5i+yGGynm9Eu/0aDKoa7ORIKb1Lw7rjI6+tBLsbSONxAlLlq66FubnjSIXc5oHh5Zk4UdKqwohZFv1pTP+NySENeZF7qLT7r9iees1f/kcDYcSJ448I8osLfSSdDuF7RLmDy+vfFuNOn9uf4/86YrFKzXTsTefC8is+9TNJk9KZMTzQQjfGMFgvL5gxv8G/Hv8RZmoAAAXdSlNMcZ1AYAAZMDgKAGAD2GfH2xxGf7AgAAAAAEWVo=";
    // This payload was generated using $ fallocate -l 9M tmp.img && mkfs.vfat -F 16 tmp.img && xz tmp.img && base64 -w 0 tmp.img.xz
    const FAT16_B64: &str = "/Td6WFoAAATm1rRGAgAhARYAAAB0L+Wj//+qAqZdAHWPDgbTdHlS2l6ajdwJ0Pmp2KUzU4digb4xctraLSpcaGAZ7oaIcZY7lTOOJ+8u71+tGUzxhpPhKG7Ujm1wd8v3duajZT8aip9d8mUt9qhU6sooUnz+6DEoqGRfAJimjTtcEmhqJnBsHS8QHYGAmOKuYLgpfUwdNMzO5Ma4IaJSy+XIrmHkorkb35I4SGdCG2qDjqziptJ4FjgAPveYtmcqG4/I3ykrMlISePnNXlVdn/GxMHN5X8CnqLk0yBh6Jd3PQsFicQQ3ySXi5nQG2Psm8fYxi/1UcfU0V9PC71DdlAJOyT/xJjW2VBI69RJSvDONoeRoXZbYSjuhKfLzWNTw6t2j0R/xrYEvumJo8aW+v+qVTwQsxr8JQ2paeEzo4eGd7PNXszNFIkkR/WMv3jlFthmE6Uw3m6QK17Le44Ciwu4v3g/cAdj7nwdXlhuKOp7c5WVKmN1xodqWsDUfZ58fEkuq5Z0+KvMuzVBXdrzzr7nSuZHsxMTnHFK/V4LZPGfoZB+B6sx8UT7wmjJyl3HGgX7fv5zaAD6tJsBAAI+1GVpVOeG9oj5WZX5I9NB0VoOcBG7Mvdnoiq2XLf8u65odus6xJxhcWYbpZlJYvul2rFnk5VsFCPnH2q38+1IrdM0eWyBC+d1TPfgpZAk7gMsqbN+1O/DEvS5fqg8+S2ZCkBMO/xCT+HF4WfgLzf+VKEYPqfx83vuaMC5WwI+F84OBwGXEJVP49ZE2MQWlsO5vwXBNRwzRkRGqrWAdus6xJxhcWYbpZlJYvul2rFnk5VsFCPnH2q38+1IrdM0eWyBC+d1TPfgpZAk7gMsqbN+1O/DEvS5fqg8+S2ZCkBMO/xCT+HF4WfgLzf+VKEYPqfx83vuaMC5WwI+F84OBwGXEJVP4vHbutJ//EAErAOxzU6f9vq58MRqft40xbnCepyNf7CjLhdGVmIp+KpHyJ3X3GcAGmE2Y/div1ZAPxCVT+PWRNjEFpbDub8FwTUcM0ZERqq1gHbrOsScYXFmG6WZSWL7pdqxZ5OVbBQj5x9qt/PtSK3TNHlsgQvndUz34KWQJO4DLKmzftTvwxL0uX6oPPktmQpATDv8Qk/hxeFn4C83/lShGD6n8fN77mjAuVsCPhfODgcBlxCVT+PWRNjEFpbDub8FwTUcM0ZERqq1gHbrOsScYXFmG6WZSWL7pdqxZ5OVbBQj5x9qt/PtSK3TNHlsgQvndUz34KWQJO4DLKmzftTvwxL0uX6oPPktmQpATDv8Qk/hxeFn4C83/lShGD6n8fN77mjAuVsCPhfODgcBlxCVRDz+yn/8QASsA7HNTp/2+rnwxGp+3jTFucJ6nI1/sKMuF0ZWYin4qkfIndfcZwAaYTZj92K/VkA/EJVP49ZE2MQWlsO5vwXBNRwzRkRGqrWAdus6xJxhcWYbpZlJYvul2rFnk5VsFCPnH2q38+1IrdM0eWyBC+d1TPfgpZAk7gMsqbN+1O/DEvS5fqg8+S2ZCkBMO/xCT+HF4WfgLzf+VKEYPqfx83vuaMC5WwI+F84OBwGXEJVP49ZE2MQWlsO5vwXBNRwzRkRGqrWAdus6xJxhcWYbpZlJYvul2rFnk5VsFCPnH2q38+1IrdM0eWyBC+d1TPfgpZAk7gMsqbN+1O/DEvS5fqg8+S2ZCkBMO/xCT+HF4WfgLzf+VKEYPqfx83vuaMC5WwI+F84OBwGXEJVEPP7Kf/xABKwDsc1On/b6ufDEan7eNMW5wnqcjX+woy4XRlZiKfiqR8id19xnABphNmP3Yr9WQD8QlU/j1kTYxBaWw7m/BcE1HDNGREaqtYB26zrEnGFxZhulmUli+6XasWeTlWwUI+cfarfz7Uit0zR5bIEL53VM9+ClkCTuAyyps37U78MS9Ll+qDz5LZkKQEw7/EJP4cXhZ+AvN/5UoRg+p/Hze+5owLlbAj4Xzg4HAZcQlU/j1kTYxBaWw7m/BcE1HDNGREaqtYB26zrEnGFxZhulmUli+6XasWeTlWwUI+cfarfz7Uit0zR5bIEL53VM9+ClkCTuAyyps37U78MS9Ll+qDz5LZkKQEw7/EJP4cXhZ+AvN/5UoRg+p/Hze+5owLlbAj4Xzg4HAZcQlUQ8/spADIQCZAOxzU6f9vq58MRqft40xbnCepyNf7CjLhdGVmIp+KpHyJ3X3GcAGmE2Y/div1ZAPxCVT+PWRNjEFpbDub8FwTUcM0ZERqq1gHbrOsScYXFmG6WZSWL7pdqxZ5OVbBQj5x9qt/PtSK3TNHlsgQvndUz34KWQJO4DLKmzftTvwxL0uX6oPPktmQpATDv8Qk/hxeFn4C80nO7JyAACCFFieVrDI2gAB9A2AgMAEj/U9dbHEZ/sCAAAAAARZWg==";
    // This payload was generated using $ fallocate -l 100k tmp.img && mkfs.vfat -F 12 tmp.img && xz tmp.img && base64 -w 0 tmp.img.xz
    const FAT12_B64: &str = "/Td6WFoAAATm1rRGAgAhARYAAAB0L+Wj4Y//AYZdAHWPDgbTdHlS2l6ajdwJ0O4PJDQvK+dBHn2NUXHuYQJCxvrRN+F6NpYnHHKaI36U1sARdgLSUykeUEwotF2UDzndDvYdJQ2ObzIHSNlXIDdEsfwDXB0tfy2W62gmex2ohMLSrqANKqIe1hpzpmSect01DmZuUkL22rKCxMPir2PWY95ZP2/578UvoJm0jZPWKczzGU/kLnXAeuX34slcp1gZYV3wRNh/uWX1S5jhwWsh0wACw/hjqnUiNFnrgKwgVbNmfSWc+6fACYJsXvSnmSa3nMsuSu124aouP7jQaUa0x1q4hkeOSTdPdOZwTlZg3fCryw40Hs7Uq+/vpirRg0ctsIeq/se6bt9MvpvbNI9IDDI3aMfKSIV73twH1EP8vt2NKN4akzXFbR3G4tsNMrqmLbGKDr9HNnoGKXOaanGFmCs1zrys6BE0bPqqI8rLpiWk0CoE0q7XH+aT63W8q6T0+1510WwB2mPy5F+eyUv1gdUuX/BmTDegeSOCEKCKKtvDALDwAAAAAEJN36OIB6UEAAGiA4CgBgA1UlqyscRn+wIAAAAABFla";

    #[test]
    fn boot_record_fat32() {
        let mut fat_data = decode_base64_xz_data(FAT32_B64);

        let vbr = unsafe { &*{ fat_data.as_mut_ptr() as *mut BaseVolumeBootRecord } };

        let reference_vbr = BaseVolumeBootRecord {
            _jmp_boot: [0xeb, 0x58, 0x90],
            oem_name: *b"mkfs.fat",
            bytes_per_sector: 512u16.into(),
            sectors_per_cluster: 1u8,
            reserved_sec_count: 32u16.into(),
            num_fats: 2u8,
            root_entry_count: 0.into(),
            total_sectors16: 192.into(),
            media: 0xf8,
            fat_size16: 0u16.into(),
            sectors_per_track: 0x0010.into(),
            num_heads: 0002.into(),
            hidden_sectors: 0.into(),
            total_sectors32: 0.into(),
        };

        assert_eq!(vbr.get_fat_type(), FATType::FAT32);
        assert_eq!(vbr, &reference_vbr);

        let fat32vbr = unsafe { vbr.into::<Fat32VBR>() }.unwrap();

        let mut reference_f32vbr = Fat32VBR {
            base: reference_vbr,
            fat_size_32: 2.into(),
            extflags: 0.into(),
            fs_ver: 0.into(),
            root_cluster: 2.into(),
            fs_info_sec: 1.into(),
            bkp_boot_sec: 6.into(),
            _reserved: [0; 12],
            vol: crate::VolData {
                drive_num: 0x80,
                _reserved: 0x0,
                ext_boot_sig: 0x29,
                vol_id: 0xaaff55aa.into(),
                vol_label: *b"NO NAME    ",
                file_system_type: *b"FAT32   ",
            },
            boot_code: [0; 420],
            boot_sig: [0x55, 0xaa],
        };

        reference_f32vbr.boot_code[..29].copy_from_slice(&BOOT_CODE32);
        reference_f32vbr.boot_code[29..129].copy_from_slice(&BOOT_STR);

        assert_eq!(&reference_f32vbr, fat32vbr);
        assert!(fat32vbr.check_signatures());

        let bytes_per_sector: u16 = vbr.bytes_per_sector.into();
        let fsinfo = unsafe {
            &mut *{ fat_data.as_mut_ptr().offset(bytes_per_sector as isize) as *mut FSInfo }
        };

        let fsinfo_reference = FSInfo {
            _lead_sig: *b"RRaA",
            _reserved: [0; 480],
            _struc_sig: *b"rrAa",
            free_count: 152.into(),
            nxt_free: 5.into(),
            _reserved2: [0; 12],
            _trail_sig: [0x00, 0x00, 0x55, 0xaa],
        };

        assert!(fsinfo.check_signatures());
        assert_eq!(&fsinfo_reference, fsinfo);
        assert_eq!(vbr.get_fat_size(), 2);
        assert_eq!(vbr.get_n_clusters(), 156);
    }

    #[test]
    fn fat32_crawl_dir() {
        let mut fat_data = decode_base64_xz_data(FAT32_B64);

        let vbr = unsafe { &*{ fat_data.as_mut_ptr() as *mut BaseVolumeBootRecord } };
        let fat32vbr = unsafe { vbr.into::<Fat32VBR>() }.unwrap();
        let bytes_per_sector: u16 = vbr.bytes_per_sector.into();

        let (fat_offset, _) = fat32vbr.get_fat_area();
        let fat_size32: u32 = fat32vbr.fat_size_32.into();
        let num_fats = (fat_size32 as usize) * (bytes_per_sector as usize);
        let fat_area: &[FAT32Entry] = unsafe {
            &*{
                slice_from_raw_parts(
                    fat_data.as_ptr().offset(fat_offset) as *mut FAT32Entry,
                    num_fats,
                )
            }
        };

        let root_dir_offset = vbr.get_root_dir_offset();
        let root_dir = unsafe {
            &*{
                slice_from_raw_parts(
                    fat_data.as_ptr().offset(root_dir_offset) as *const DirEntry,
                    vbr.get_cluster_size() / size_of::<DirEntry>(),
                )
            }
        };

        let dir_entry_reference = DirEntry {
            name: *b"EFI        ",
            attr: 0x10,
            ntres: 0,
            crt_time_tenth: 0,
            crt_time: 26489u16.into(),
            crt_date: 22418u16.into(),
            lst_acc_date: 22418u16.into(),
            fst_clus_hi: 0u16.into(),
            wrt_time: 26489u16.into(),
            wrt_date: 22418u16.into(),
            fst_clus_lo: 3u16.into(),
            file_size: 0u32.into(),
        };

        assert_eq!(&dir_entry_reference, &root_dir[0]);
        assert_eq!(&root_dir[1].name[0], &0x0);
        assert!(&fat_area[0].is_end_of_chain());
        let efi_dir_cluster = dir_entry_reference.get_first_cluster();
        assert_eq!(efi_dir_cluster, 0x3u32);

        let efi_entries_cluster = &fat_area[efi_dir_cluster as usize];
        assert!(efi_entries_cluster.is_end_of_chain());
        let efi_dir = unsafe {
            &*{
                slice_from_raw_parts(
                    fat_data.as_ptr().offset(
                        vbr.get_first_data_cluster_offset()
                            + ((root_dir[0].get_first_cluster() as isize - 2isize)
                                * vbr.get_cluster_size() as isize),
                    ) as *const DirEntry,
                    vbr.get_cluster_size() / size_of::<DirEntry>(),
                )
            }
        };
        assert_eq!(&efi_dir[0].name, &*b".          ");
        assert_eq!(&efi_dir[1].name, &*b"..         ");
        assert_eq!(&efi_dir[2].name, &*b"FEDORA     ");
        assert_eq!(&efi_dir[3].name[0], &0x0u8);
    }

    #[test]
    fn boot_record_fat16() {
        let mut fat_data = decode_base64_xz_data(FAT16_B64);

        let vbr = unsafe { &*{ fat_data.as_mut_ptr() as *mut BaseVolumeBootRecord } };

        let reference_vbr = BaseVolumeBootRecord {
            _jmp_boot: [0xeb, 0x3C, 0x90],
            oem_name: *b"mkfs.fat",
            bytes_per_sector: 512u16.into(),
            sectors_per_cluster: 4u8,
            reserved_sec_count: 4u16.into(),
            num_fats: 2u8,
            root_entry_count: 512.into(),
            total_sectors16: 18432.into(),
            media: 0xf8,
            fat_size16: 20u16.into(),
            sectors_per_track: 32.into(),
            num_heads: 0002.into(),
            hidden_sectors: 0.into(),
            total_sectors32: 0.into(),
        };

        assert_eq!(vbr.get_fat_type(), FATType::FAT16);
        assert_eq!(vbr, &reference_vbr);

        let fat16vbr = unsafe { vbr.into::<Fat1216VBR>() }.unwrap();

        let mut fat16vbr_reference = Fat1216VBR {
            base: reference_vbr,
            vol: crate::VolData {
                drive_num: 0x80,
                _reserved: 0x0,
                ext_boot_sig: 0x29,
                vol_id: 0xc125c3a6.into(),
                vol_label: *b"NO NAME    ",
                file_system_type: *b"FAT16   ",
            },
            boot_code: [0; 448],
            boot_sig: [0x55, 0xaa],
        };

        fat16vbr_reference.boot_code[..29].copy_from_slice(&BOOT_CODE16);
        fat16vbr_reference.boot_code[29..129].copy_from_slice(&BOOT_STR);

        assert_eq!(&fat16vbr_reference, fat16vbr);
        assert!(fat16vbr.check_signatures());
        assert_eq!(vbr.get_fat_size(), 20);
        assert_eq!(vbr.get_n_clusters(), 4597);

        let (_, fat_area_length) = fat16vbr.get_fat_area();
        let bytes_per_sector: u16 = vbr.bytes_per_sector.into();
        let fat_size16: u16 = vbr.fat_size16.into();

        assert!(fat_size16 as usize * bytes_per_sector as usize <= fat_area_length);
    }

    #[test]
    fn fat16_crawl_dir() {
        let mut fat_data = decode_base64_xz_data(FAT16_B64);

        let vbr = unsafe { &*{ fat_data.as_mut_ptr() as *mut BaseVolumeBootRecord } };
        let fat16vbr = unsafe { vbr.into::<Fat1216VBR>() }.unwrap();

        let root_dir_offset = vbr.get_root_dir_offset();
        let root_dir = unsafe {
            &*{
                slice_from_raw_parts(
                    fat_data.as_ptr().offset(root_dir_offset) as *const DirEntry,
                    vbr.get_cluster_size() / size_of::<DirEntry>(),
                )
            }
        };

        let dir_entry_reference = DirEntry {
            name: *b"EFI        ",
            attr: 0x10,
            ntres: 0,
            crt_time_tenth: 0,
            crt_time: 36664u16.into(),
            crt_date: 22634u16.into(),
            lst_acc_date: 22634u16.into(),
            fst_clus_hi: 0u16.into(),
            wrt_time: 36664u16.into(),
            wrt_date: 22634u16.into(),
            fst_clus_lo: 2u16.into(),
            file_size: 0u32.into(),
        };

        assert_eq!(&dir_entry_reference, &root_dir[0]);
        assert_eq!(&root_dir[1].name[0], &0x0);

        let efi_dir_cluster = dir_entry_reference.get_first_cluster();
        assert_eq!(efi_dir_cluster, 0x2u32);

        let (fat_offset, _) = fat16vbr.get_fat_area();
        let fat_size16: u16 = vbr.fat_size16.into();
        let bytes_per_sector: u16 = vbr.bytes_per_sector.into();
        let num_fats = (fat_size16 as usize) * (bytes_per_sector as usize);
        let fat16_entries = unsafe {
            &*{
                slice_from_raw_parts(
                    fat_data.as_ptr().offset(fat_offset as isize) as *const FAT16Entry,
                    num_fats,
                )
            }
        };

        assert!(fat16_entries[root_dir[0].get_first_cluster() as usize].is_end_of_chain());
        let efi_dir = unsafe {
            &*{
                slice_from_raw_parts(
                    fat_data.as_ptr().offset(
                        vbr.get_first_data_cluster_offset()
                            + ((root_dir[0].get_first_cluster() as isize - 2isize)
                                * vbr.get_cluster_size() as isize),
                    ) as *const DirEntry,
                    vbr.get_cluster_size() / size_of::<DirEntry>(),
                )
            }
        };

        assert_eq!(&efi_dir[0].name, &*b".          ");
        assert_eq!(&efi_dir[1].name, &*b"..         ");
        assert_eq!(&efi_dir[2].name, &*b"FEDORA     ");
        assert_eq!(&efi_dir[3].name[0], &0x0u8);
    }

    #[test]
    fn boot_record_fat12() {
        let mut fat_data = decode_base64_xz_data(FAT12_B64);

        let vbr = unsafe { &*{ fat_data.as_mut_ptr() as *mut BaseVolumeBootRecord } };

        let reference_vbr = BaseVolumeBootRecord {
            _jmp_boot: [0xeb, 0x3C, 0x90],
            oem_name: *b"mkfs.fat",
            bytes_per_sector: 512u16.into(),
            sectors_per_cluster: 4u8,
            reserved_sec_count: 1u16.into(),
            num_fats: 2u8,
            root_entry_count: 512.into(),
            total_sectors16: 192.into(),
            media: 0xf8,
            fat_size16: 1u16.into(),
            sectors_per_track: 16.into(),
            num_heads: 0002.into(),
            hidden_sectors: 0.into(),
            total_sectors32: 0.into(),
        };

        assert_eq!(vbr.get_fat_type(), FATType::FAT12);
        assert_eq!(vbr, &reference_vbr);

        let fat12vbr = unsafe { vbr.into::<Fat1216VBR>() }.unwrap();

        let mut fat12vbr_reference = Fat1216VBR {
            base: reference_vbr,
            vol: crate::VolData {
                drive_num: 0x80,
                _reserved: 0x0,
                ext_boot_sig: 0x29,
                vol_id: 0xf08754a7.into(),
                vol_label: *b"NO NAME    ",
                file_system_type: *b"FAT12   ",
            },
            boot_code: [0; 448],
            boot_sig: [0x55, 0xaa],
        };

        fat12vbr_reference.boot_code[..29].copy_from_slice(&BOOT_CODE16);
        fat12vbr_reference.boot_code[29..129].copy_from_slice(&BOOT_STR);

        assert_eq!(&fat12vbr_reference, fat12vbr);
        assert!(fat12vbr.check_signatures());
        assert_eq!(vbr.get_fat_size(), 1);
        assert_eq!(vbr.get_n_clusters(), 47);

        let (_, fat_area_length) = fat12vbr.get_fat_area();
        let bytes_per_sector: u16 = vbr.bytes_per_sector.into();
        let fat_size16: u16 = vbr.fat_size16.into();

        assert!(fat_size16 as usize * bytes_per_sector as usize <= fat_area_length);
    }

    #[test]
    fn fat12_crawl_dir() {
        let mut fat_data = decode_base64_xz_data(FAT12_B64);
        let vbr = unsafe { &*{ fat_data.as_mut_ptr() as *mut BaseVolumeBootRecord } };
        let fat12vbr = unsafe { vbr.into::<Fat1216VBR>() }.unwrap();

        let (fat_offset, _) = fat12vbr.get_fat_area();
        let bytes_per_sector: u16 = vbr.bytes_per_sector.into();
        let fat_size16: u16 = vbr.fat_size16.into();

        let root_dir_offset = vbr.get_root_dir_offset();
        let root_dir = unsafe {
            &*{
                slice_from_raw_parts(
                    fat_data.as_ptr().offset(root_dir_offset) as *const DirEntry,
                    vbr.get_cluster_size() / size_of::<DirEntry>(),
                )
            }
        };

        let dir_entry_reference = DirEntry {
            name: *b"EFI        ",
            attr: 0x10,
            ntres: 0,
            crt_time_tenth: 0,
            crt_time: 30478u16.into(),
            crt_date: 22611u16.into(),
            lst_acc_date: 22611u16.into(),
            fst_clus_hi: 0u16.into(),
            wrt_time: 30478u16.into(),
            wrt_date: 22611u16.into(),
            fst_clus_lo: 2u16.into(),
            file_size: 0u32.into(),
        };

        assert_eq!(&dir_entry_reference, &root_dir[0]);
        assert_eq!(&root_dir[1].name[0], &0x0);

        let efi_dir_cluster = dir_entry_reference.get_first_cluster();
        assert_eq!(efi_dir_cluster, 0x2u32);

        let fat12area = FAT12Area(unsafe {
            &*{
                slice_from_raw_parts(
                    fat_data.as_ptr().offset(fat_offset),
                    bytes_per_sector as usize * fat_size16 as usize,
                )
            }
        });
        let efi_entries_cluster = fat12area.get_entry(efi_dir_cluster as usize).unwrap();
        assert!(efi_entries_cluster.is_end_of_chain());
        let efi_dir = unsafe {
            &*{
                slice_from_raw_parts(
                    fat_data.as_ptr().offset(
                        vbr.get_first_data_cluster_offset()
                            + ((root_dir[0].get_first_cluster() as isize - 2isize)
                                * vbr.get_cluster_size() as isize),
                    ) as *const DirEntry,
                    vbr.get_cluster_size() / size_of::<DirEntry>(),
                )
            }
        };

        assert_eq!(&efi_dir[0].name, &*b".          ");
        assert_eq!(&efi_dir[1].name, &*b"..         ");
        assert_eq!(&efi_dir[2].name, &*b"FEDORA     ");
        assert_eq!(&efi_dir[3].name[0], &0x0u8);
    }

    #[test]
    fn fat12_area() {
        let area = FAT12Area(&[0xAAu8, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00]);

        assert_eq!(area.len(), 4);
        assert_eq!(area.get_entry(0).unwrap(), FAT12Entry(0xaab));
        assert_eq!(area.get_entry(1).unwrap(), FAT12Entry(0xbcc));
        assert_eq!(area.get_entry(2).unwrap(), FAT12Entry(0xdde));
        assert_eq!(area.get_entry(3).unwrap(), FAT12Entry(0xeff));
        assert!(area.get_entry(4).is_none());
    }
}
